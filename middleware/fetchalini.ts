import { Middleware } from "@nuxt/types";
import state from "~/state";

const fetchalini: Middleware = async ({ $axios }) => {
    state.projects = await $axios.$get(
        `${process.env.SERVER_URL}/project`
    );

};

export default fetchalini;