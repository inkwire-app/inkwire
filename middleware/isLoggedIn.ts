import { Middleware } from "@nuxt/types";
import state from "~/state";

const isLoggedIn: Middleware = ({ redirect, $axios }) => {
    let accessToken = "";
    let refreshToken = "";

    if (process.browser) {
        accessToken = window.localStorage.getItem("accessToken") || "";
        refreshToken = window.localStorage.getItem("refreshToken") || "";

        state.accessToken = accessToken;
        state.refreshToken = refreshToken;

        // @ts-ignore
        clearInterval(state.refreshTimer);

        state.refreshTimer = window.setInterval(async () => {
            const refreshResponse = await $axios.$post(
                `${process.env.SERVER_URL}/login/refresh`,
                {},
                {
                    // @ts-ignore
                    Authorization: `Bearer ${state.refreshToken}`,
                }
            );

            state.accessToken = refreshResponse.auth_token;
            state.refreshToken = refreshResponse.refresh_token;

            if (process.browser) {
                localStorage.setItem("accessToken", state.accessToken);
                localStorage.setItem("refreshToken", state.refreshToken);
            }

            $axios.setHeader(
                "Authorization",
                `Bearer ${refreshResponse.auth_token}`
            );
        }, 600000);

        if (accessToken) {
            $axios.setHeader("Authorization", `Bearer ${accessToken}`);
        }
    }

    if (!state.isLoggedIn) redirect("/");
};

export default isLoggedIn;
