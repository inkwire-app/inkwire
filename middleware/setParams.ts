import { Middleware } from "@nuxt/types";
import state from "~/state";

const setParams: Middleware = ({ params }) => {
    state.params = {
        sourceId: Number(params.sourceId) || -1,
        projectId: Number(params.projectId) || -1,
    };
};

export default setParams;
