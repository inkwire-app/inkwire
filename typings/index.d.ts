export interface IAPIAnnotation {
    start_char: number;
    end_char: number;
    text: string;
    id?: number;
}

type EntityLabelT =
    | "ORG"
    | "PERSON"
    | "DATE"
    | "GPE"
    | "MONEY"
    | "TIME"
    | "PERCENT"
    | "CARDINAL";

export interface IEntity extends IAPIAnnotation {
    label: EntityLabelT;
    sent: string;
}

export interface IAPIAuthor {
    full_name: string;

    id: number;
    source_id: number;
}

export interface IAPISource {
    source_type: string;

    title: string;
    url: string;
    access_date: string;
    edition: string;
    volume: string;

    authors: IAPIAuthor[];

    publisher: string;
    publisher_location: string;
    publish_date: string;

    text: string;
    citation: string;
    last_edited: string;

    id: number;
    project_id: string;
    user_id: string;

    annotations: IAPIAnnotation[];
}

export interface IAPIProject {
    id: number;
    name: string;

    citation_style: string;

    sources: IAPISource[];

    last_edited: string;
}

export interface IAPIUser {
    id: number;

    projects: IAPIProject[];
}

export interface ICiteAuthor {
    type: string;
    first?: string;
    last?: string;
    full?: string;
}

export interface ICitationPayload {
    authors: ICiteAuthor[];
    access_date: string;
    source_type: "website" | "book";

    url?: string;

    publisher: string;
    publisher_location: string;
    date: string;
}

export interface IEditingSource extends ICitationPayload {
    title: string;
    id: number;
    thumbnail?: string;
}
