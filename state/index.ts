import Vue from "vue";
import {
    IAPIAnnotation,
    IAPIProject,
    IAPISource,
    IEditingSource,
} from "~/typings";

const state = new Vue({
    data() {
        return {
            accessToken: "",
            refreshToken: "",

            projects: [] as IAPIProject[],

            projectDialog: false,
            sourceDialog: false,

            annotationDialog: {
                show: false,
                annotationIndex: -1,
            },

            params: {
                projectId: -1,
                sourceId: -1,
            },

            editingProject: {} as Partial<IAPIProject>,
            editingSource: {} as Partial<IEditingSource>,
            editingPaper: {} as any,

            refreshTimer: -1 as number,
        };
    },
    computed: {
        isLoggedIn(): boolean {
            return this.accessToken.length > 0 && this.refreshToken.length > 0;
        },
        currentProject(): IAPIProject | undefined {
            return this.projects
                .filter((p) => !!p)
                .find((p) => p.id === this.params.projectId);
        },
        currentSource(): IAPISource | undefined {
            return this.currentProject?.sources
                .filter((s) => !!s)
                .find((s) => s.id === this.params.sourceId);
        },
        projectAnnotations(): IAPIAnnotation[] {
            const arr: IAPIAnnotation[] = [];
            this.currentProject?.sources.forEach((s) =>
                arr.push(...s.annotations)
            );
            return arr;
        },
    },
    methods: {
        getAnnotationsByProjectId(id: number): IAPIAnnotation[] {
            const arr: IAPIAnnotation[] = [];

            const project = this.projects
                .filter((p) => !!p)
                .find((p) => p.id === id);

            project?.sources.forEach((s) => arr.push(...s.annotations));

            return arr;
        },
        getAnnotationsByProjectIndex(index: number): IAPIAnnotation[] {
            const arr: IAPIAnnotation[] = [];

            const project = this.projects[index];
            project.sources.forEach((s) => arr.push(...s.annotations));

            return arr;
        },
        getTextFromAnnotation(
            sourceId: number,
            start: number,
            end: number
        ): string | undefined {
            return this.currentProject?.sources
                .find((s) => s.id === sourceId)
                ?.text.substring(start, end);
        },
    },
});

process.env.NODE_ENV === "development" &&
    Object.defineProperty(window, "__state", { get: () => state });

export default state;
